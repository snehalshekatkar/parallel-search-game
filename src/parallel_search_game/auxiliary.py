# -*- coding: utf-8 -*-
#
# Copyright (C) 2020 Snehal M. Shekatkar <snehal@inferred.co>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import re
import os
import sys
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib
from mpl_toolkits.axes_grid1 import make_axes_locatable
import parallel_search_game as psg

def all_max_attempts(num_boxes=100, num_players=100, tot_permutations=100, 
    strategy='key', escape_strategy='randomclosed', increment=1, 
    initial_choice='natural', switch_prob=0.0, offset=0, be_stupid=False, 
    out_dir = "psg_output_data", plot_heatmaps=False, plot_histograms=False):

    r"""A convenience wrapper for `play_game` to try all possible values of the 
    max_attempts. See the documentation of the `play_game` for more information.  
    This function directly saves the following histogram in the path 'out_dir' 
    with meanigful filename, and returns the path of the output file.

    parameters
    ----------

    hist2d : :class:`numpy.ndarray`
        This will be a normalized histogram containing the probabilities of
        the total number of winners for ``tot_permutations`` number of games.
        number_of_winners varies along axis=1 and max_attempts varies along
        axis = 0. 

    """

    hist2d = []
    for max_attempts in range(1, num_boxes + 1):
        sys.stdout.write(f"\rmax_attemtps = {max_attempts}")
        sys.stdout.flush()
        tot_used_attempts, pmf = psg.play_game(num_boxes, num_players, 
            max_attempts, tot_permutations, strategy, escape_strategy, 
            increment, initial_choice, switch_prob, offset, be_stupid)
        # Each PMF needs to be of length num_boxes+1 for plotting. Hence we
        # append zeros if its length is smaller
        if len(pmf) < num_boxes + 1:
            pmf = np.concatenate((pmf, np.zeros(num_boxes+1 - len(pmf))))
        # Normalize the histogram        
        hist2d.append(pmf/tot_permutations)
    print()
    
    # Check if the output directory exists, and if not, create it
    if out_dir != "" and not os.path.isdir(out_dir):
        os.makedirs(out_dir)
    # Generate a meaningful filename and pathname
    filename = generate_filename('exact_winners', strategy, escape_strategy,
        num_boxes, num_players, increment, initial_choice, switch_prob, 
        offset, be_stupid)
    if out_dir !='':
        path = f'{out_dir}/{filename}.dat'
    else:
        path = f'{filename}.dat'
    # Save the histogram
    np.savetxt(path, hist2d, fmt = '%f')

    if plot_heatmaps:
        pmf = np.loadtxt(path)
        # Plot exact winners heatmap
        plot_heatmap(pmf, num_boxes, num_players, filename)
        # Plot minimum winners heatmap
        cmf = 1-np.cumsum(pmf, axis=1) + pmf
        filename = generate_filename('min_winners', strategy, escape_strategy,
        num_boxes, num_players, increment, initial_choice, switch_prob, 
        offset, be_stupid)
        plot_heatmap(cmf, num_boxes, num_players, filename)

    if plot_histograms:
        pmf = np.loadtxt(path)
        # Plot exact winners heatmap
        plot_histogram(pmf, num_boxes, num_players, filename)
        # Plot minimum winners heatmap
        cmf = 1-np.cumsum(pmf, axis=1) + pmf
        filename = generate_filename('min_winners', strategy, escape_strategy,
        num_boxes, num_players, increment, initial_choice, switch_prob, 
        offset, be_stupid)
        plot_histogram(cmf, num_boxes, num_players, filename)

    return path

def generate_filename(plot_type='exact_winners', strategy='key', 
    escape_strategy='randomclosed', num_boxes=100, num_players=100, 
    increment = None, initial_choice='natural', switch_prob=0, 
    offset = None, be_stupid=False):

    r"""Generate a meaniful output filename based on the parameters.
    """

    s = plot_type 

    if strategy == 'goyalsaks':
        filename = s + '_' + strategy + '_' + f'N{num_boxes}_n{num_players}'
        return filename

    if strategy == 'ADI':
        filename = s + '_' + strategy + '_' + f'N{num_boxes}_n{num_players}'
        return filename

    if strategy == 'box':
        if increment == None:
            raise ValueError('Increment value must be passed for the box strategy.')
        if increment <= 0:
            raise ValueError("Increment must be greater than zero for the "
            "box strategy")
            ''
        s += '_' + strategy + str(increment)
    else:
        s += '_' + strategy

    # Decide whether the escape_strategy is to be added or not
    if (strategy == 'key' and (num_players < num_boxes or 
        initial_choice != 'natural')):
        s += '_' + escape_strategy
    if (strategy == 'box' and num_boxes % increment == 0 and increment > 1):
        s += '_' + escape_strategy

    s += '_N' + str(num_boxes) + '_n' + str(num_players)

    if strategy not in ['randomclosed', 'random']:
        if initial_choice == 'offset':
            if offset == None:
                raise ValueError("Offset value must be passed when initial choice is 'offset'")
            if offset <= 0:
                raise ValueError('offset must be Greater than zero for offset' 
                ' type initial choice')
                
            s += '_' + initial_choice + str(offset) 
        else:
            s += '_' + initial_choice 

    if be_stupid and not (strategy=='key' and initial_choice=='natural'): 
        s += '_stupid'
    if switch_prob > 0.:
        s += f'_switch{switch_prob}'

    return s

def get_params(filename):
    r"""Extract all the parameter values from the filename of the datafile. 

    Parameters
    ----------
    filename : ``str`` 
        Name of the file from which to extract the parameters. This should be 
        a 'bare' filename and not relative/absolute filepath. 
        
    Returns
    -------
    params : tuple of parameters
        The following tuple is returned. 
        (strategy, escape_strategy, increment, num_boxes, num_players, 
        initial_choice, offset)
        When filename doesn't contain escape_strategy, 'randomclosed' 
        escape_strategy is returned. When strategy is other than 'box', 
        increment = 1 is returned. When initial_choice is offset, offset = 0 
        is returned.
    
    """
    # If the filename ends with extension 'csv' or 'dat', slash it
    if re.search('.csv$|.dat$', filename):
        filename = filename[:-4]

    if '_stupid' in filename:
        filename = filename.replace('_stupid', '')
        stupid = True
    else:
        stupid = False
    if re.search('^exact_winners', filename):
        s = re.findall('^exact_winners_(.+)', filename)[0]
    elif re.search('^min_winners', filename):
        s = re.findall('^min_winners_(.+)', filename)[0]
    else:
        s = filename
    s = s.split('_')
    if len(s) == 3 and s[0] in ['ADI', 'goyalsaks']:
        strategy=s[0]
        escape_strategy='NA'
        increment = 1
        num_boxes = int(re.findall('^N(.+)', s[1])[0])
        num_players = int(re.findall('^n(.+)', s[2])[0])
        initial_choice = 'natural'
        offset = 0
        params = (strategy, escape_strategy, increment, num_boxes, num_players, 
            initial_choice, stupid, offset)
    if len(s) == 4:
        strategy, increment = re.findall('([a-zA-Z]+)([0-9]*)$', s[0])[0]
        if len(increment) > 0:
            increment = int(increment)
        else:
            increment = 1
        num_boxes = int(re.findall('^N(.+)', s[1])[0])
        num_players = int(re.findall('^n(.+)', s[2])[0])
        initial_choice, offset = re.findall('([a-zA-Z]+)([0-9]*)$', s[3])[0]
        if len(offset) > 0:
            offset = int(offset)
        else:
            offset = 0
        # Escape strategy is not applicable (NA) here
        escape_strategy = 'NA'
        params = (strategy, escape_strategy, increment, num_boxes, num_players, 
            initial_choice, stupid, offset)
    elif len(s) == 5:
        strategy, increment = re.findall('([a-zA-Z]+)([0-9]*)$', s[0])[0]
        if len(increment) > 0:
            increment = int(increment)
        else:
            increment = 1
        escape_strategy = s[1]
        num_boxes = int(re.findall('^N(.+)', s[2])[0])
        num_players = int(re.findall('^n(.+)', s[3])[0])
        initial_choice, offset = re.findall('([a-zA-Z]+)([0-9]*)$', s[4])[0]
        if len(offset) > 0:
            offset = int(offset)
        else:
            offset = 0
        params = (strategy, escape_strategy, increment, num_boxes, num_players, 
            initial_choice, stupid, offset)

    return params

def plot_histogram(plot_array, num_boxes, num_players, filename, cmap=plt.cm.jet, 
    azim = 300, elev = 45, out_dir='psg_histograms'):

    r"""Plot the 3d-bar plot of the 2d histogram.
    
    parameters
    ----------
    plot_array : :class:`numpy.ndarray`
    num_boxes  : int (optional, default: ``100``)
    num_players: int (optional, default: ``100``)
    filename : ``str`` 
        The name of the output file. The actual name will be 'heatmap_filename'.
    cmap : matplotlib colormap (optional, default: plt.cm.jet)
    azim : float (optional, default: 300)
        The azimuthal angle for the 3d view, should be between 0 and 360. 
    elev : float (optional, default: 45)
        The elevation angle for the 3d view, should be between 0 and 90.
    out_dir : ``str`` (optional, default: ``"psg_output_data"`` )
        The output directory. If the directory doesn't exist, it will be 
        created.
    """

    # If the output directory doesn't exist, create it
    if out_dir != '' and not os.path.isdir(out_dir):
        os.makedirs(out_dir)
    # Check if the filename already contains an extension, and add .png 
    # if it doesn't
    if not re.search(r'png$|jpg$|jpeg$|pdf$', filename):
        filename += '.png'
    # Set the axes
    fig = plt.figure(figsize = (9, 7))
    ax = fig.add_subplot(111, projection = '3d')
    _x = np.arange(num_boxes + 1)
    _y = np.arange(1, num_boxes + 1)
    _xx, _yy = np.meshgrid(_x, _y)
    x, y = _xx.ravel(), _yy.ravel()

    plot_array = np.array(plot_array).ravel()
    bottom = np.zeros_like(plot_array)
    width, depth = 0.2, 0.2
    cmap = cmap
    ax.bar3d(x, y, bottom, width, depth, plot_array, shade = True,
        color = cmap(plot_array), alpha = 1, zsort = 'max')
    ax.azim = azim
    ax.elev = elev
    
    plt.xlabel(r'$w$', fontsize = 20, fontname = 'Serif')
    plt.ylabel(r'$a$', fontsize = 20, fontname = 'Serif')
    plt.title(f'N = {num_boxes}, n = {num_players}\n', fontsize = 20)
    fig.colorbar(plt.cm.ScalarMappable(cmap = cmap), shrink = 0.5, aspect = 15)
    plt.tight_layout()
    if out_dir != '':
        path = fr'{out_dir}/histogram_{filename}'
    else:
        path = fr'histogram_{filename}'
    plt.savefig(path)
    plt.close()
    return path

def plot_heatmap(plot_array, num_boxes, num_players, filename, 
    cmap=plt.cm.jet, out_dir='psg_heatmaps'):
    r"""Plot heatmap for the plot_array and save the output at appropriate
    Location. 
    
    """
    # If the output directory doesn't exist, create it
    if out_dir != '' and not os.path.isdir(out_dir):
        os.makedirs(out_dir)
    # Check if the filename already contains an extension, and add .png 
    # if it doesn't
    if not re.search(r'png$|jpg$|jpeg$|pdf$', filename):
        filename += '.png'
    plt.imshow(plot_array, origin = 'lower', aspect='equal',
        cmap = cmap)
    plt.xlabel(r'$w$', fontsize = 20, fontname = 'Serif')
    plt.ylabel(r'$a$', fontsize = 20, fontname = 'Serif')
    plt.title(f'N = {num_boxes}, n = {num_players}\n', fontsize = 20)
    plt.colorbar()
    plt.tight_layout()
    if out_dir != '':
        path = fr'{out_dir}/heatmap_{filename}'
    else:
        path = fr'heatmap_{filename}'
    plt.savefig(path, dpi=200)
    plt.close()
    return path

def efficiency(pmf, num_boxes, num_players, beta=2):
    r"""Compute the efficiency of a given strategy using its
    Probability Mass Function (PMF) and weighting index beta.
    """
    
    # Construct the array of ratios w/a
    x = np.arange(num_boxes+1)
    y = np.arange(1,num_boxes+1)
    _x, _y = np.meshgrid(x, y)
    ratio = (_x/num_players)/(_y/num_boxes)
    efficiency = np.sum(ratio**beta * pmf )
    
    return efficiency

def get_combo(num_boxes=100, num_players=100, strategy='key', 
    escape_strategy='randomclosed', increment=1, initial_choice='natural', 
    switch_prob=0.0, offset=0, be_stupid=False, beta=2, out_dir='psg_output_data'):
    r"""Get a list containing initial_choice, strategy, and its efficiency.
    See the documentation of play_game for the meaning of the parameters.
    """
    combo = [initial_choice, ]
    # Generate the filename
    filename = generate_filename(plot_type='exact_winners', 
    strategy=strategy, escape_strategy=escape_strategy, num_boxes=num_boxes, 
    num_players=num_players, increment=increment , initial_choice=initial_choice, 
    offset=offset)
    print(filename)
    try:
        x = np.loadtxt(f"{out_dir}/{filename}.dat")
    except OSError:
        print("The required output file not found. Generating the same using "
        "10000 permutations. This may take a few minutes, be patient!")
        all_max_attempts(num_boxes=num_boxes, num_players=num_players, 
        tot_permutations=10000, strategy=strategy, 
        escape_strategy=escape_strategy, increment=increment, 
        initial_choice=initial_choice, switch_prob=switch_prob, offset=offset, 
        be_stupid=be_stupid, out_dir=out_dir)
        x = np.loadtxt(f"{out_dir}/{filename}.dat")
    # Generate the filename for the corresponding random strategy
    filename = generate_filename(plot_type='exact_winners', 
    strategy='randomclosed', num_boxes=num_boxes, num_players=num_players, 
    increment=increment, offset=offset)
    print(filename)
    if os.path.isfile(f"{out_dir}/{filename}.dat"):
        y = np.loadtxt(f"{out_dir}/{filename}.dat")
    else:
        print("The output file for the corresponding random strategy doesn't "
        "exist. Generating the same using 10000 permutations. This may take a"
        " few minutes, be patient!")
        all_max_attempts(num_boxes=num_boxes, num_players=num_players, 
        tot_permutations=10000, strategy='randomclosed', 
        initial_choice=initial_choice, switch_prob=switch_prob, offset=offset, 
        be_stupid=be_stupid, out_dir=out_dir)
        y = np.loadtxt(f"{out_dir}/{filename}.dat")
    # For a given initial_choice, normalization factor is the efficiency of 
    # the corresponding randomclosed strategy
    norm = efficiency(y, num_boxes, num_players, beta=beta)
    combo.extend([strategy, efficiency(x,num_boxes,num_players,beta=beta)/norm])

    return combo

def get_acronym(filename):
    r"""Get a symbolic string for a given filename
    """

    if 'ADI' in filename:
        return 'ADI'
    if 'goyalsaks' in filename:
        return 'Goyal-Saks'
    # Make necessary modifications to the filename 
    if not re.search('.+dat$', filename):
        filename += '.dat'
    if re.search('^min_winners_.+', filename):
        filename = filename.replace('min_', 'exact_')
    if not re.search('^exact_winners_.+', filename):
        filename = 'exact_winners_' + filename

    # Extract the parameters from the filename
    (strategy, escape_strategy, increment, num_boxes, num_players, 
            initial_choice, stupid, offset) = get_params(filename)

    init_dict = {'natural' : 'a', 'offset' : 'b', 'random' : 'c'}
    strategy_dict = {'key' : 'KS', 'box' : 'BS', 'randomclosed' : 'RS', 
        'random' : 'PRS'}
    escape_dict = {'key' : 'KS', 'boxclosed' : 'BS', 'randomclosed' : 'RS'}

    name = f'{strategy_dict.get(strategy)}'
    if initial_choice == 'natural':
        name += '^0'

    if strategy == 'box':
        name += f'{increment}'

    if stupid:
        name += '_stupid'
    return name

if __name__ == '__main__':
    print(generate_filename(strategy='box', initial_choice='offset', offset = 3, be_stupid=True, increment = 2))
