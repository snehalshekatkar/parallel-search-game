! -*- coding: utf-8 -*-
!
! Copyright (C) 2020 Snehal M. Shekatkar <snehal@inferred.co>
!
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>.

Module gs
    implicit none

    contains
    
    subroutine play_game(num_boxes, num_players, max_attempts, tot_permutations, &
        & tot_used_attempts, pmf)
        implicit none
        integer*8 :: num_boxes, num_players, d, tot_permutations
        integer*8 :: i, j, s, t, l, ix, attempts, max_attempts, tot_winners
        integer*8 :: tot_used_attempts(tot_permutations), box_opened(num_boxes)
        integer*8 :: box(num_boxes), success(num_players), st_set(num_boxes), pmf(0:num_players)
        real*8 :: sp
    
!f2py intent(in) :: num_boxes, num_players, max_attempts, tot_permutations
!f2py intent(out) :: tot_used_attempts, pmf
    
        d = int(real(num_boxes, 8)/num_players)
        pmf = 0
        tot_used_attempts = 0
        ! Loop over realizations
        DO ix = 1, tot_permutations
            ! Initial condition :
            ! -1 indicates an empty box. We first put all num_players keys in first 
            ! num_players boxes, and keep the remaining boxes empty. Then we shuffle 
            ! this to get a random initial condition
            box = -1 
            Do i = 1, num_players
                box(i) = i
            Enddo
            ! shuffle
            call random_permute(num_boxes, box)
            !print'(20(I0,1X))',box
    
            success = 0
            ! Start the game
            Do i = 1, num_players
                ! All boxes are closed at the start
                box_opened = 0
                ! Player i starts from the first box of bin i
                ! The index of that box is as written below
                j = d*(i-1) + 1
                box_opened(j) = 1
                attempts = 1
                ! Initialize s, t, l and attempts. Here s and t are the first and last
                ! elements in the st_set and l is the size of the st_set
                s = j
                t = j
                l = 1
                ! Construct the corresponding [s,t] set
                ! For the convenience of implementation, the array size for st_set is 
                ! always num_boxes, but we use first l elements as the actual set
                st_set(1) = j
                ! Keep playing till the player gets the key or runs out of attempts
                do while (box(j) /= i .and. attempts < max_attempts)
                    ! Compute the length of the st-set depending upon whether s<=t or not
                    if (s <= t)then
                        l = abs(s-t)+1
                    else
                        l = num_boxes-s+1+t 
                    endif
                    ! Compute the surplus for the current st_set
                    sp = surplus(l, st_set, box, num_players, num_boxes)
                    ! If surplus is negative, we go to the next box
                    if (sp < 0.) then
                        j = mod(j+1, num_boxes)
                        if (j == 0) then
                            j = num_boxes
                        endif
                        ! Here we append j to the st_set
                        l = l+1
                        st_set(l) = j
                    ! Otherwise, the box contains some key, we go to the bin
                    ! corresponding to that key
                    else
                        j = d*(box(j)-1) + 1
                        l = 1
                        st_set(l) = j
                    endif
                    if (box_opened(j) == 0)then
                        box_opened(j) = 1
                    endif
                    attempts = attempts + 1
                    tot_used_attempts(ix) = tot_used_attempts(ix) + 1
                enddo
                ! Assign success if the key is located in max_attempts
                if (box(j) == i) success(i) = 1
            Enddo
            tot_winners = sum(success)
            pmf(tot_winners) = pmf(tot_winners) + 1
        ENDDO
    End subroutine play_game
    !=================================================================
    
    function surplus(l, st_set, box, num_players, num_boxes)
        !Calculate the surplus for a given st_set st_set. 
        !The argument "check" can be used to print something by making 
        !it True, and putting a condition based on its value inside the 
        !function. This allows printing for only certain calls to the function. 
        implicit none
        integer*8 :: i, l, num_players, num_boxes, tot_occupied, d
        integer*8 :: st_set(num_boxes), box(num_boxes)
        real*8 :: surplus
    
        tot_occupied = 0
        DO i = 1, l
            if (box(st_set(i)) /= -1) then
                tot_occupied = tot_occupied + 1            
            endif
        ENDDO
        d = num_boxes/num_players
        surplus = tot_occupied - real(l, 8)/d
    end function surplus
    !=================================================================
    
    function rand_int(a, b)
        ! Return a random integer between a and b (inclusive) uniformly randomly
        implicit none
        integer*8 :: a, b, rand_int
        real*8 :: r
        
        call random_number(r)
        rand_int = a + Floor((b-a+1)*r)
    
    end function rand_int
    
    !=================================================================
    ! random permutation
    subroutine random_permute(n, x)
        ! Algorithm taken from Knuth, vol2
        implicit none
        integer*8 :: i, j, temp, n, x(n)
    
        DO j = n, 1, -1
            i = rand_int(int(1, 8), j)
            temp = x(i)
            x(i) = x(j)
            x(j) = temp
        ENDDO
    end subroutine random_permute
    
    !=================================================================
End module gs
